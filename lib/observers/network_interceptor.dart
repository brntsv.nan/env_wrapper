import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import 'package:uuid/uuid.dart';

import '../data/monitoring_entries.dart';
import '../services/log_vault.dart';

class EagleDioInterceptor extends Interceptor {
  final Map<String, MonitoringEntryNetworkCall> monitoring = {};

  @override
  void onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) {
    final reqId = const Uuid().v1();
    final entry = MonitoringEntry.networkCall(
      id: reqId,
      start: DateTime.now(),
      uri: options.uri,
      requestHeaders: options.headers,
      requestQuery: options.queryParameters,
      request: options.data,
      logTimestamp: DateTime.now(),
    );
    monitoring[reqId] = entry as MonitoringEntryNetworkCall;
    GetIt.I.get<LogVault>().addEntry(entry);
    options.extra["reqId"] = reqId;
    handler.next(options);
  }

  @override
  void onResponse(
    Response response,
    ResponseInterceptorHandler handler,
  ) {
    final id = response.requestOptions.extra["reqId"];
    final entry = monitoring[id];
    if (entry != null) {
      entry.responseHeaders = response.headers.map
          .map((key, value) => MapEntry(key, value.join("; ")));
      entry.response = response.data;
      entry.end = DateTime.now();
    }
    handler.next(response);
  }

  @override
  void onError(
    DioException err,
    ErrorInterceptorHandler handler,
  ) {
    final id = err.requestOptions.extra["reqId"];
    final entry = monitoring[id];
    if (entry != null) {
      entry.isErr = true;
      entry.responseHeaders = err.response?.headers.map
          .map((key, value) => MapEntry(key, value.join("; ")));
      entry.response = err.response?.data;
      entry.end = DateTime.now();
    }
    handler.next(err);
  }
}

extension EagleNetworking on Dio {
  addEagleMonitoring() {
    interceptors.add(EagleDioInterceptor());
  }
}
