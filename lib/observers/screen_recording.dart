import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

import '../core/screen_recorder.dart';

class QaToolsRecorder extends StatefulWidget {
  const QaToolsRecorder({
    super.key,
    required this.child,
  });

  final Widget child;

  static final ValueNotifier<bool> isScreenRecording = ValueNotifier(false);
  static final ValueNotifier<double> processingState = ValueNotifier(0.0);
  static final ValueNotifier<bool> isProcessing = ValueNotifier(false);

  @override
  State<QaToolsRecorder> createState() => _QaToolsRecorderState();
}

class _QaToolsRecorderState extends State<QaToolsRecorder> {
  exportAndShare() async {
    QaToolsRecorder.isProcessing.value = true;
    /* final gif = await recordingController.export(
      (progress) {
        QaToolsRecorder.processingState.value = progress;
      },
    );
    Directory tempDir = await getTemporaryDirectory();
    final f = File(tempDir.path + "/" + "recording.gif");
    await f.writeAsBytes(gif!, flush: true);
    await Share.shareXFiles([XFile(f.path)]);*/

    QaToolsRecorder.isProcessing.value = false;
  }

  @override
  void initState() {
    QaToolsRecorder.isScreenRecording.addListener(() {
      setState(() {
        if (QaToolsRecorder.isScreenRecording.value) {
          recordingController.start();
        } else {
          recordingController.stop();
          Future.delayed(
            const Duration(milliseconds: 100),
            () {
              exportAndShare();
            },
          );
        }
      });
    });
    super.initState();
  }

  late ScreenRecorderController recordingController = ScreenRecorderController(
    pixelRatio: 1,
    skipFramesBetweenCaptures: 10,
    onEachFrame: captureFrame,
  );

  Future captureFrame(Uint8List value) async {
    Directory tempDir = await getTemporaryDirectory();
    final recordingDir = await Directory("${tempDir.path}/recording").create();
    final f = File(
        "${recordingDir.path}/${DateFormat("HH_mm_ss").format(DateTime.now())}.png");
    await f.writeAsBytes(value, flush: true);
  }

  @override
  Widget build(BuildContext context) {
    return ScreenRecorder(
      controller: recordingController,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: widget.child,
    );
  }
}
