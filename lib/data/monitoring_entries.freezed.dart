// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'monitoring_entries.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

StackFrame _$StackFrameFromJson(Map<String, dynamic> json) {
  return _StackFrameCtor.fromJson(json);
}

/// @nodoc
mixin _$StackFrame {
  String? get funcName => throw _privateConstructorUsedError;
  int? get line => throw _privateConstructorUsedError;
  int? get column => throw _privateConstructorUsedError;
  String get path => throw _privateConstructorUsedError;

  /// Serializes this StackFrame to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of StackFrame
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $StackFrameCopyWith<StackFrame> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StackFrameCopyWith<$Res> {
  factory $StackFrameCopyWith(
          StackFrame value, $Res Function(StackFrame) then) =
      _$StackFrameCopyWithImpl<$Res, StackFrame>;
  @useResult
  $Res call({String? funcName, int? line, int? column, String path});
}

/// @nodoc
class _$StackFrameCopyWithImpl<$Res, $Val extends StackFrame>
    implements $StackFrameCopyWith<$Res> {
  _$StackFrameCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of StackFrame
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? funcName = freezed,
    Object? line = freezed,
    Object? column = freezed,
    Object? path = null,
  }) {
    return _then(_value.copyWith(
      funcName: freezed == funcName
          ? _value.funcName
          : funcName // ignore: cast_nullable_to_non_nullable
              as String?,
      line: freezed == line
          ? _value.line
          : line // ignore: cast_nullable_to_non_nullable
              as int?,
      column: freezed == column
          ? _value.column
          : column // ignore: cast_nullable_to_non_nullable
              as int?,
      path: null == path
          ? _value.path
          : path // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$StackFrameCtorImplCopyWith<$Res>
    implements $StackFrameCopyWith<$Res> {
  factory _$$StackFrameCtorImplCopyWith(_$StackFrameCtorImpl value,
          $Res Function(_$StackFrameCtorImpl) then) =
      __$$StackFrameCtorImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? funcName, int? line, int? column, String path});
}

/// @nodoc
class __$$StackFrameCtorImplCopyWithImpl<$Res>
    extends _$StackFrameCopyWithImpl<$Res, _$StackFrameCtorImpl>
    implements _$$StackFrameCtorImplCopyWith<$Res> {
  __$$StackFrameCtorImplCopyWithImpl(
      _$StackFrameCtorImpl _value, $Res Function(_$StackFrameCtorImpl) _then)
      : super(_value, _then);

  /// Create a copy of StackFrame
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? funcName = freezed,
    Object? line = freezed,
    Object? column = freezed,
    Object? path = null,
  }) {
    return _then(_$StackFrameCtorImpl(
      funcName: freezed == funcName
          ? _value.funcName
          : funcName // ignore: cast_nullable_to_non_nullable
              as String?,
      line: freezed == line
          ? _value.line
          : line // ignore: cast_nullable_to_non_nullable
              as int?,
      column: freezed == column
          ? _value.column
          : column // ignore: cast_nullable_to_non_nullable
              as int?,
      path: null == path
          ? _value.path
          : path // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$StackFrameCtorImpl implements _StackFrameCtor {
  _$StackFrameCtorImpl(
      {required this.funcName,
      required this.line,
      required this.column,
      required this.path});

  factory _$StackFrameCtorImpl.fromJson(Map<String, dynamic> json) =>
      _$$StackFrameCtorImplFromJson(json);

  @override
  final String? funcName;
  @override
  final int? line;
  @override
  final int? column;
  @override
  final String path;

  @override
  String toString() {
    return 'StackFrame(funcName: $funcName, line: $line, column: $column, path: $path)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StackFrameCtorImpl &&
            (identical(other.funcName, funcName) ||
                other.funcName == funcName) &&
            (identical(other.line, line) || other.line == line) &&
            (identical(other.column, column) || other.column == column) &&
            (identical(other.path, path) || other.path == path));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, funcName, line, column, path);

  /// Create a copy of StackFrame
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$StackFrameCtorImplCopyWith<_$StackFrameCtorImpl> get copyWith =>
      __$$StackFrameCtorImplCopyWithImpl<_$StackFrameCtorImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$StackFrameCtorImplToJson(
      this,
    );
  }
}

abstract class _StackFrameCtor implements StackFrame {
  factory _StackFrameCtor(
      {required final String? funcName,
      required final int? line,
      required final int? column,
      required final String path}) = _$StackFrameCtorImpl;

  factory _StackFrameCtor.fromJson(Map<String, dynamic> json) =
      _$StackFrameCtorImpl.fromJson;

  @override
  String? get funcName;
  @override
  int? get line;
  @override
  int? get column;
  @override
  String get path;

  /// Create a copy of StackFrame
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$StackFrameCtorImplCopyWith<_$StackFrameCtorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

MonitoringEntry _$MonitoringEntryFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType']) {
    case 'networkCall':
      return MonitoringEntryNetworkCall.fromJson(json);
    case 'storageOperation':
      return MonitoringEntryStarageOperation.fromJson(json);
    case 'exception':
      return MonitoringEntryException.fromJson(json);
    case 'textLog':
      return MonitoringEntryTextLog.fromJson(json);
    case 'blocEvent':
      return MonitoringBlocEventLog.fromJson(json);

    default:
      throw CheckedFromJsonException(json, 'runtimeType', 'MonitoringEntry',
          'Invalid union type "${json['runtimeType']}"!');
  }
}

/// @nodoc
mixin _$MonitoringEntry {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)
        networkCall,
    required TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)
        storageOperation,
    required TResult Function(Object object, List<StackFrame> frames) exception,
    required TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)
        textLog,
    required TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)
        blocEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult? Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult? Function(Object object, List<StackFrame> frames)? exception,
    TResult? Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult? Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult Function(Object object, List<StackFrame> frames)? exception,
    TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MonitoringEntryNetworkCall value) networkCall,
    required TResult Function(MonitoringEntryStarageOperation value)
        storageOperation,
    required TResult Function(MonitoringEntryException value) exception,
    required TResult Function(MonitoringEntryTextLog value) textLog,
    required TResult Function(MonitoringBlocEventLog value) blocEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult? Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult? Function(MonitoringEntryException value)? exception,
    TResult? Function(MonitoringEntryTextLog value)? textLog,
    TResult? Function(MonitoringBlocEventLog value)? blocEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult Function(MonitoringEntryException value)? exception,
    TResult Function(MonitoringEntryTextLog value)? textLog,
    TResult Function(MonitoringBlocEventLog value)? blocEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  /// Serializes this MonitoringEntry to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MonitoringEntryCopyWith<$Res> {
  factory $MonitoringEntryCopyWith(
          MonitoringEntry value, $Res Function(MonitoringEntry) then) =
      _$MonitoringEntryCopyWithImpl<$Res, MonitoringEntry>;
}

/// @nodoc
class _$MonitoringEntryCopyWithImpl<$Res, $Val extends MonitoringEntry>
    implements $MonitoringEntryCopyWith<$Res> {
  _$MonitoringEntryCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
abstract class _$$MonitoringEntryNetworkCallImplCopyWith<$Res> {
  factory _$$MonitoringEntryNetworkCallImplCopyWith(
          _$MonitoringEntryNetworkCallImpl value,
          $Res Function(_$MonitoringEntryNetworkCallImpl) then) =
      __$$MonitoringEntryNetworkCallImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {Uri uri,
      String id,
      DateTime logTimestamp,
      DateTime start,
      dynamic request,
      DateTime? end,
      bool? isErr,
      Map<String, dynamic> requestQuery,
      Map<String, dynamic> requestHeaders,
      Map<String, String>? responseHeaders,
      dynamic response});
}

/// @nodoc
class __$$MonitoringEntryNetworkCallImplCopyWithImpl<$Res>
    extends _$MonitoringEntryCopyWithImpl<$Res,
        _$MonitoringEntryNetworkCallImpl>
    implements _$$MonitoringEntryNetworkCallImplCopyWith<$Res> {
  __$$MonitoringEntryNetworkCallImplCopyWithImpl(
      _$MonitoringEntryNetworkCallImpl _value,
      $Res Function(_$MonitoringEntryNetworkCallImpl) _then)
      : super(_value, _then);

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uri = null,
    Object? id = null,
    Object? logTimestamp = null,
    Object? start = null,
    Object? request = freezed,
    Object? end = freezed,
    Object? isErr = freezed,
    Object? requestQuery = null,
    Object? requestHeaders = null,
    Object? responseHeaders = freezed,
    Object? response = freezed,
  }) {
    return _then(_$MonitoringEntryNetworkCallImpl(
      uri: null == uri
          ? _value.uri
          : uri // ignore: cast_nullable_to_non_nullable
              as Uri,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      logTimestamp: null == logTimestamp
          ? _value.logTimestamp
          : logTimestamp // ignore: cast_nullable_to_non_nullable
              as DateTime,
      start: null == start
          ? _value.start
          : start // ignore: cast_nullable_to_non_nullable
              as DateTime,
      request: freezed == request
          ? _value.request
          : request // ignore: cast_nullable_to_non_nullable
              as dynamic,
      end: freezed == end
          ? _value.end
          : end // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      isErr: freezed == isErr
          ? _value.isErr
          : isErr // ignore: cast_nullable_to_non_nullable
              as bool?,
      requestQuery: null == requestQuery
          ? _value.requestQuery
          : requestQuery // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
      requestHeaders: null == requestHeaders
          ? _value.requestHeaders
          : requestHeaders // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
      responseHeaders: freezed == responseHeaders
          ? _value.responseHeaders
          : responseHeaders // ignore: cast_nullable_to_non_nullable
              as Map<String, String>?,
      response: freezed == response
          ? _value.response
          : response // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MonitoringEntryNetworkCallImpl extends MonitoringEntryNetworkCall {
  _$MonitoringEntryNetworkCallImpl(
      {required this.uri,
      required this.id,
      required this.logTimestamp,
      required this.start,
      this.request,
      this.end,
      this.isErr,
      required this.requestQuery,
      required this.requestHeaders,
      this.responseHeaders,
      this.response,
      final String? $type})
      : $type = $type ?? 'networkCall',
        super._();

  factory _$MonitoringEntryNetworkCallImpl.fromJson(
          Map<String, dynamic> json) =>
      _$$MonitoringEntryNetworkCallImplFromJson(json);

  @override
  Uri uri;
  @override
  String id;
  @override
  DateTime logTimestamp;
  @override
  DateTime start;
  @override
  dynamic request;
  @override
  DateTime? end;
  @override
  bool? isErr;
  @override
  Map<String, dynamic> requestQuery;
  @override
  Map<String, dynamic> requestHeaders;
  @override
  Map<String, String>? responseHeaders;
  @override
  dynamic response;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'MonitoringEntry.networkCall(uri: $uri, id: $id, logTimestamp: $logTimestamp, start: $start, request: $request, end: $end, isErr: $isErr, requestQuery: $requestQuery, requestHeaders: $requestHeaders, responseHeaders: $responseHeaders, response: $response)';
  }

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$MonitoringEntryNetworkCallImplCopyWith<_$MonitoringEntryNetworkCallImpl>
      get copyWith => __$$MonitoringEntryNetworkCallImplCopyWithImpl<
          _$MonitoringEntryNetworkCallImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)
        networkCall,
    required TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)
        storageOperation,
    required TResult Function(Object object, List<StackFrame> frames) exception,
    required TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)
        textLog,
    required TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)
        blocEvent,
  }) {
    return networkCall(uri, id, logTimestamp, start, request, end, isErr,
        requestQuery, requestHeaders, responseHeaders, response);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult? Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult? Function(Object object, List<StackFrame> frames)? exception,
    TResult? Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult? Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
  }) {
    return networkCall?.call(uri, id, logTimestamp, start, request, end, isErr,
        requestQuery, requestHeaders, responseHeaders, response);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult Function(Object object, List<StackFrame> frames)? exception,
    TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
    required TResult orElse(),
  }) {
    if (networkCall != null) {
      return networkCall(uri, id, logTimestamp, start, request, end, isErr,
          requestQuery, requestHeaders, responseHeaders, response);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MonitoringEntryNetworkCall value) networkCall,
    required TResult Function(MonitoringEntryStarageOperation value)
        storageOperation,
    required TResult Function(MonitoringEntryException value) exception,
    required TResult Function(MonitoringEntryTextLog value) textLog,
    required TResult Function(MonitoringBlocEventLog value) blocEvent,
  }) {
    return networkCall(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult? Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult? Function(MonitoringEntryException value)? exception,
    TResult? Function(MonitoringEntryTextLog value)? textLog,
    TResult? Function(MonitoringBlocEventLog value)? blocEvent,
  }) {
    return networkCall?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult Function(MonitoringEntryException value)? exception,
    TResult Function(MonitoringEntryTextLog value)? textLog,
    TResult Function(MonitoringBlocEventLog value)? blocEvent,
    required TResult orElse(),
  }) {
    if (networkCall != null) {
      return networkCall(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$MonitoringEntryNetworkCallImplToJson(
      this,
    );
  }
}

abstract class MonitoringEntryNetworkCall extends MonitoringEntry {
  factory MonitoringEntryNetworkCall(
      {required Uri uri,
      required String id,
      required DateTime logTimestamp,
      required DateTime start,
      dynamic request,
      DateTime? end,
      bool? isErr,
      required Map<String, dynamic> requestQuery,
      required Map<String, dynamic> requestHeaders,
      Map<String, String>? responseHeaders,
      dynamic response}) = _$MonitoringEntryNetworkCallImpl;
  MonitoringEntryNetworkCall._() : super._();

  factory MonitoringEntryNetworkCall.fromJson(Map<String, dynamic> json) =
      _$MonitoringEntryNetworkCallImpl.fromJson;

  Uri get uri;
  set uri(Uri value);
  String get id;
  set id(String value);
  DateTime get logTimestamp;
  set logTimestamp(DateTime value);
  DateTime get start;
  set start(DateTime value);
  dynamic get request;
  set request(dynamic value);
  DateTime? get end;
  set end(DateTime? value);
  bool? get isErr;
  set isErr(bool? value);
  Map<String, dynamic> get requestQuery;
  set requestQuery(Map<String, dynamic> value);
  Map<String, dynamic> get requestHeaders;
  set requestHeaders(Map<String, dynamic> value);
  Map<String, String>? get responseHeaders;
  set responseHeaders(Map<String, String>? value);
  dynamic get response;
  set response(dynamic value);

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$MonitoringEntryNetworkCallImplCopyWith<_$MonitoringEntryNetworkCallImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$MonitoringEntryStarageOperationImplCopyWith<$Res> {
  factory _$$MonitoringEntryStarageOperationImplCopyWith(
          _$MonitoringEntryStarageOperationImpl value,
          $Res Function(_$MonitoringEntryStarageOperationImpl) then) =
      __$$MonitoringEntryStarageOperationImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {StarageOperationType storage,
      String storageName,
      String key,
      String value,
      DateTime logTimestamp});
}

/// @nodoc
class __$$MonitoringEntryStarageOperationImplCopyWithImpl<$Res>
    extends _$MonitoringEntryCopyWithImpl<$Res,
        _$MonitoringEntryStarageOperationImpl>
    implements _$$MonitoringEntryStarageOperationImplCopyWith<$Res> {
  __$$MonitoringEntryStarageOperationImplCopyWithImpl(
      _$MonitoringEntryStarageOperationImpl _value,
      $Res Function(_$MonitoringEntryStarageOperationImpl) _then)
      : super(_value, _then);

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? storage = null,
    Object? storageName = null,
    Object? key = null,
    Object? value = null,
    Object? logTimestamp = null,
  }) {
    return _then(_$MonitoringEntryStarageOperationImpl(
      storage: null == storage
          ? _value.storage
          : storage // ignore: cast_nullable_to_non_nullable
              as StarageOperationType,
      storageName: null == storageName
          ? _value.storageName
          : storageName // ignore: cast_nullable_to_non_nullable
              as String,
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      value: null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
      logTimestamp: null == logTimestamp
          ? _value.logTimestamp
          : logTimestamp // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MonitoringEntryStarageOperationImpl
    extends MonitoringEntryStarageOperation {
  _$MonitoringEntryStarageOperationImpl(
      {required this.storage,
      required this.storageName,
      required this.key,
      required this.value,
      required this.logTimestamp,
      final String? $type})
      : $type = $type ?? 'storageOperation',
        super._();

  factory _$MonitoringEntryStarageOperationImpl.fromJson(
          Map<String, dynamic> json) =>
      _$$MonitoringEntryStarageOperationImplFromJson(json);

  @override
  StarageOperationType storage;
  @override
  String storageName;
  @override
  String key;
  @override
  String value;
  @override
  DateTime logTimestamp;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'MonitoringEntry.storageOperation(storage: $storage, storageName: $storageName, key: $key, value: $value, logTimestamp: $logTimestamp)';
  }

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$MonitoringEntryStarageOperationImplCopyWith<
          _$MonitoringEntryStarageOperationImpl>
      get copyWith => __$$MonitoringEntryStarageOperationImplCopyWithImpl<
          _$MonitoringEntryStarageOperationImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)
        networkCall,
    required TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)
        storageOperation,
    required TResult Function(Object object, List<StackFrame> frames) exception,
    required TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)
        textLog,
    required TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)
        blocEvent,
  }) {
    return storageOperation(storage, storageName, key, value, logTimestamp);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult? Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult? Function(Object object, List<StackFrame> frames)? exception,
    TResult? Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult? Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
  }) {
    return storageOperation?.call(
        storage, storageName, key, value, logTimestamp);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult Function(Object object, List<StackFrame> frames)? exception,
    TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
    required TResult orElse(),
  }) {
    if (storageOperation != null) {
      return storageOperation(storage, storageName, key, value, logTimestamp);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MonitoringEntryNetworkCall value) networkCall,
    required TResult Function(MonitoringEntryStarageOperation value)
        storageOperation,
    required TResult Function(MonitoringEntryException value) exception,
    required TResult Function(MonitoringEntryTextLog value) textLog,
    required TResult Function(MonitoringBlocEventLog value) blocEvent,
  }) {
    return storageOperation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult? Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult? Function(MonitoringEntryException value)? exception,
    TResult? Function(MonitoringEntryTextLog value)? textLog,
    TResult? Function(MonitoringBlocEventLog value)? blocEvent,
  }) {
    return storageOperation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult Function(MonitoringEntryException value)? exception,
    TResult Function(MonitoringEntryTextLog value)? textLog,
    TResult Function(MonitoringBlocEventLog value)? blocEvent,
    required TResult orElse(),
  }) {
    if (storageOperation != null) {
      return storageOperation(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$MonitoringEntryStarageOperationImplToJson(
      this,
    );
  }
}

abstract class MonitoringEntryStarageOperation extends MonitoringEntry {
  factory MonitoringEntryStarageOperation(
      {required StarageOperationType storage,
      required String storageName,
      required String key,
      required String value,
      required DateTime logTimestamp}) = _$MonitoringEntryStarageOperationImpl;
  MonitoringEntryStarageOperation._() : super._();

  factory MonitoringEntryStarageOperation.fromJson(Map<String, dynamic> json) =
      _$MonitoringEntryStarageOperationImpl.fromJson;

  StarageOperationType get storage;
  set storage(StarageOperationType value);
  String get storageName;
  set storageName(String value);
  String get key;
  set key(String value);
  String get value;
  set value(String value);
  DateTime get logTimestamp;
  set logTimestamp(DateTime value);

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$MonitoringEntryStarageOperationImplCopyWith<
          _$MonitoringEntryStarageOperationImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$MonitoringEntryExceptionImplCopyWith<$Res> {
  factory _$$MonitoringEntryExceptionImplCopyWith(
          _$MonitoringEntryExceptionImpl value,
          $Res Function(_$MonitoringEntryExceptionImpl) then) =
      __$$MonitoringEntryExceptionImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Object object, List<StackFrame> frames});
}

/// @nodoc
class __$$MonitoringEntryExceptionImplCopyWithImpl<$Res>
    extends _$MonitoringEntryCopyWithImpl<$Res, _$MonitoringEntryExceptionImpl>
    implements _$$MonitoringEntryExceptionImplCopyWith<$Res> {
  __$$MonitoringEntryExceptionImplCopyWithImpl(
      _$MonitoringEntryExceptionImpl _value,
      $Res Function(_$MonitoringEntryExceptionImpl) _then)
      : super(_value, _then);

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? object = null,
    Object? frames = null,
  }) {
    return _then(_$MonitoringEntryExceptionImpl(
      object: null == object ? _value.object : object,
      frames: null == frames
          ? _value.frames
          : frames // ignore: cast_nullable_to_non_nullable
              as List<StackFrame>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MonitoringEntryExceptionImpl extends MonitoringEntryException {
  _$MonitoringEntryExceptionImpl(
      {required this.object, required this.frames, final String? $type})
      : $type = $type ?? 'exception',
        super._();

  factory _$MonitoringEntryExceptionImpl.fromJson(Map<String, dynamic> json) =>
      _$$MonitoringEntryExceptionImplFromJson(json);

  @override
  Object object;
  @override
  List<StackFrame> frames;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'MonitoringEntry.exception(object: $object, frames: $frames)';
  }

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$MonitoringEntryExceptionImplCopyWith<_$MonitoringEntryExceptionImpl>
      get copyWith => __$$MonitoringEntryExceptionImplCopyWithImpl<
          _$MonitoringEntryExceptionImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)
        networkCall,
    required TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)
        storageOperation,
    required TResult Function(Object object, List<StackFrame> frames) exception,
    required TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)
        textLog,
    required TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)
        blocEvent,
  }) {
    return exception(object, frames);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult? Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult? Function(Object object, List<StackFrame> frames)? exception,
    TResult? Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult? Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
  }) {
    return exception?.call(object, frames);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult Function(Object object, List<StackFrame> frames)? exception,
    TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
    required TResult orElse(),
  }) {
    if (exception != null) {
      return exception(object, frames);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MonitoringEntryNetworkCall value) networkCall,
    required TResult Function(MonitoringEntryStarageOperation value)
        storageOperation,
    required TResult Function(MonitoringEntryException value) exception,
    required TResult Function(MonitoringEntryTextLog value) textLog,
    required TResult Function(MonitoringBlocEventLog value) blocEvent,
  }) {
    return exception(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult? Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult? Function(MonitoringEntryException value)? exception,
    TResult? Function(MonitoringEntryTextLog value)? textLog,
    TResult? Function(MonitoringBlocEventLog value)? blocEvent,
  }) {
    return exception?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult Function(MonitoringEntryException value)? exception,
    TResult Function(MonitoringEntryTextLog value)? textLog,
    TResult Function(MonitoringBlocEventLog value)? blocEvent,
    required TResult orElse(),
  }) {
    if (exception != null) {
      return exception(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$MonitoringEntryExceptionImplToJson(
      this,
    );
  }
}

abstract class MonitoringEntryException extends MonitoringEntry {
  factory MonitoringEntryException(
      {required Object object,
      required List<StackFrame> frames}) = _$MonitoringEntryExceptionImpl;
  MonitoringEntryException._() : super._();

  factory MonitoringEntryException.fromJson(Map<String, dynamic> json) =
      _$MonitoringEntryExceptionImpl.fromJson;

  Object get object;
  set object(Object value);
  List<StackFrame> get frames;
  set frames(List<StackFrame> value);

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$MonitoringEntryExceptionImplCopyWith<_$MonitoringEntryExceptionImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$MonitoringEntryTextLogImplCopyWith<$Res> {
  factory _$$MonitoringEntryTextLogImplCopyWith(
          _$MonitoringEntryTextLogImpl value,
          $Res Function(_$MonitoringEntryTextLogImpl) then) =
      __$$MonitoringEntryTextLogImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String text,
      EagleLogSeverity severity,
      String id,
      DateTime logTimestamp});
}

/// @nodoc
class __$$MonitoringEntryTextLogImplCopyWithImpl<$Res>
    extends _$MonitoringEntryCopyWithImpl<$Res, _$MonitoringEntryTextLogImpl>
    implements _$$MonitoringEntryTextLogImplCopyWith<$Res> {
  __$$MonitoringEntryTextLogImplCopyWithImpl(
      _$MonitoringEntryTextLogImpl _value,
      $Res Function(_$MonitoringEntryTextLogImpl) _then)
      : super(_value, _then);

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? text = null,
    Object? severity = null,
    Object? id = null,
    Object? logTimestamp = null,
  }) {
    return _then(_$MonitoringEntryTextLogImpl(
      text: null == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
      severity: null == severity
          ? _value.severity
          : severity // ignore: cast_nullable_to_non_nullable
              as EagleLogSeverity,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      logTimestamp: null == logTimestamp
          ? _value.logTimestamp
          : logTimestamp // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MonitoringEntryTextLogImpl extends MonitoringEntryTextLog {
  _$MonitoringEntryTextLogImpl(
      {required this.text,
      required this.severity,
      required this.id,
      required this.logTimestamp,
      final String? $type})
      : $type = $type ?? 'textLog',
        super._();

  factory _$MonitoringEntryTextLogImpl.fromJson(Map<String, dynamic> json) =>
      _$$MonitoringEntryTextLogImplFromJson(json);

  @override
  String text;
  @override
  EagleLogSeverity severity;
  @override
  String id;
  @override
  DateTime logTimestamp;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'MonitoringEntry.textLog(text: $text, severity: $severity, id: $id, logTimestamp: $logTimestamp)';
  }

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$MonitoringEntryTextLogImplCopyWith<_$MonitoringEntryTextLogImpl>
      get copyWith => __$$MonitoringEntryTextLogImplCopyWithImpl<
          _$MonitoringEntryTextLogImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)
        networkCall,
    required TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)
        storageOperation,
    required TResult Function(Object object, List<StackFrame> frames) exception,
    required TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)
        textLog,
    required TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)
        blocEvent,
  }) {
    return textLog(text, severity, id, logTimestamp);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult? Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult? Function(Object object, List<StackFrame> frames)? exception,
    TResult? Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult? Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
  }) {
    return textLog?.call(text, severity, id, logTimestamp);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult Function(Object object, List<StackFrame> frames)? exception,
    TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
    required TResult orElse(),
  }) {
    if (textLog != null) {
      return textLog(text, severity, id, logTimestamp);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MonitoringEntryNetworkCall value) networkCall,
    required TResult Function(MonitoringEntryStarageOperation value)
        storageOperation,
    required TResult Function(MonitoringEntryException value) exception,
    required TResult Function(MonitoringEntryTextLog value) textLog,
    required TResult Function(MonitoringBlocEventLog value) blocEvent,
  }) {
    return textLog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult? Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult? Function(MonitoringEntryException value)? exception,
    TResult? Function(MonitoringEntryTextLog value)? textLog,
    TResult? Function(MonitoringBlocEventLog value)? blocEvent,
  }) {
    return textLog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult Function(MonitoringEntryException value)? exception,
    TResult Function(MonitoringEntryTextLog value)? textLog,
    TResult Function(MonitoringBlocEventLog value)? blocEvent,
    required TResult orElse(),
  }) {
    if (textLog != null) {
      return textLog(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$MonitoringEntryTextLogImplToJson(
      this,
    );
  }
}

abstract class MonitoringEntryTextLog extends MonitoringEntry {
  factory MonitoringEntryTextLog(
      {required String text,
      required EagleLogSeverity severity,
      required String id,
      required DateTime logTimestamp}) = _$MonitoringEntryTextLogImpl;
  MonitoringEntryTextLog._() : super._();

  factory MonitoringEntryTextLog.fromJson(Map<String, dynamic> json) =
      _$MonitoringEntryTextLogImpl.fromJson;

  String get text;
  set text(String value);
  EagleLogSeverity get severity;
  set severity(EagleLogSeverity value);
  String get id;
  set id(String value);
  DateTime get logTimestamp;
  set logTimestamp(DateTime value);

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$MonitoringEntryTextLogImplCopyWith<_$MonitoringEntryTextLogImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$MonitoringBlocEventLogImplCopyWith<$Res> {
  factory _$$MonitoringBlocEventLogImplCopyWith(
          _$MonitoringBlocEventLogImpl value,
          $Res Function(_$MonitoringBlocEventLogImpl) then) =
      __$$MonitoringBlocEventLogImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String sourceBlocType,
      BlocEventType type,
      String? eventType,
      DateTime logTimestamp});
}

/// @nodoc
class __$$MonitoringBlocEventLogImplCopyWithImpl<$Res>
    extends _$MonitoringEntryCopyWithImpl<$Res, _$MonitoringBlocEventLogImpl>
    implements _$$MonitoringBlocEventLogImplCopyWith<$Res> {
  __$$MonitoringBlocEventLogImplCopyWithImpl(
      _$MonitoringBlocEventLogImpl _value,
      $Res Function(_$MonitoringBlocEventLogImpl) _then)
      : super(_value, _then);

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sourceBlocType = null,
    Object? type = null,
    Object? eventType = freezed,
    Object? logTimestamp = null,
  }) {
    return _then(_$MonitoringBlocEventLogImpl(
      sourceBlocType: null == sourceBlocType
          ? _value.sourceBlocType
          : sourceBlocType // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as BlocEventType,
      eventType: freezed == eventType
          ? _value.eventType
          : eventType // ignore: cast_nullable_to_non_nullable
              as String?,
      logTimestamp: null == logTimestamp
          ? _value.logTimestamp
          : logTimestamp // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MonitoringBlocEventLogImpl extends MonitoringBlocEventLog {
  _$MonitoringBlocEventLogImpl(
      {required this.sourceBlocType,
      required this.type,
      this.eventType,
      required this.logTimestamp,
      final String? $type})
      : $type = $type ?? 'blocEvent',
        super._();

  factory _$MonitoringBlocEventLogImpl.fromJson(Map<String, dynamic> json) =>
      _$$MonitoringBlocEventLogImplFromJson(json);

  @override
  String sourceBlocType;
  @override
  BlocEventType type;
  @override
  String? eventType;
  @override
  DateTime logTimestamp;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'MonitoringEntry.blocEvent(sourceBlocType: $sourceBlocType, type: $type, eventType: $eventType, logTimestamp: $logTimestamp)';
  }

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$MonitoringBlocEventLogImplCopyWith<_$MonitoringBlocEventLogImpl>
      get copyWith => __$$MonitoringBlocEventLogImplCopyWithImpl<
          _$MonitoringBlocEventLogImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)
        networkCall,
    required TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)
        storageOperation,
    required TResult Function(Object object, List<StackFrame> frames) exception,
    required TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)
        textLog,
    required TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)
        blocEvent,
  }) {
    return blocEvent(sourceBlocType, type, eventType, logTimestamp);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult? Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult? Function(Object object, List<StackFrame> frames)? exception,
    TResult? Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult? Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
  }) {
    return blocEvent?.call(sourceBlocType, type, eventType, logTimestamp);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Uri uri,
            String id,
            DateTime logTimestamp,
            DateTime start,
            dynamic request,
            DateTime? end,
            bool? isErr,
            Map<String, dynamic> requestQuery,
            Map<String, dynamic> requestHeaders,
            Map<String, String>? responseHeaders,
            dynamic response)?
        networkCall,
    TResult Function(StarageOperationType storage, String storageName,
            String key, String value, DateTime logTimestamp)?
        storageOperation,
    TResult Function(Object object, List<StackFrame> frames)? exception,
    TResult Function(String text, EagleLogSeverity severity, String id,
            DateTime logTimestamp)?
        textLog,
    TResult Function(String sourceBlocType, BlocEventType type,
            String? eventType, DateTime logTimestamp)?
        blocEvent,
    required TResult orElse(),
  }) {
    if (blocEvent != null) {
      return blocEvent(sourceBlocType, type, eventType, logTimestamp);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(MonitoringEntryNetworkCall value) networkCall,
    required TResult Function(MonitoringEntryStarageOperation value)
        storageOperation,
    required TResult Function(MonitoringEntryException value) exception,
    required TResult Function(MonitoringEntryTextLog value) textLog,
    required TResult Function(MonitoringBlocEventLog value) blocEvent,
  }) {
    return blocEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult? Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult? Function(MonitoringEntryException value)? exception,
    TResult? Function(MonitoringEntryTextLog value)? textLog,
    TResult? Function(MonitoringBlocEventLog value)? blocEvent,
  }) {
    return blocEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(MonitoringEntryNetworkCall value)? networkCall,
    TResult Function(MonitoringEntryStarageOperation value)? storageOperation,
    TResult Function(MonitoringEntryException value)? exception,
    TResult Function(MonitoringEntryTextLog value)? textLog,
    TResult Function(MonitoringBlocEventLog value)? blocEvent,
    required TResult orElse(),
  }) {
    if (blocEvent != null) {
      return blocEvent(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$MonitoringBlocEventLogImplToJson(
      this,
    );
  }
}

abstract class MonitoringBlocEventLog extends MonitoringEntry {
  factory MonitoringBlocEventLog(
      {required String sourceBlocType,
      required BlocEventType type,
      String? eventType,
      required DateTime logTimestamp}) = _$MonitoringBlocEventLogImpl;
  MonitoringBlocEventLog._() : super._();

  factory MonitoringBlocEventLog.fromJson(Map<String, dynamic> json) =
      _$MonitoringBlocEventLogImpl.fromJson;

  String get sourceBlocType;
  set sourceBlocType(String value);
  BlocEventType get type;
  set type(BlocEventType value);
  String? get eventType;
  set eventType(String? value);
  DateTime get logTimestamp;
  set logTimestamp(DateTime value);

  /// Create a copy of MonitoringEntry
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$MonitoringBlocEventLogImplCopyWith<_$MonitoringBlocEventLogImpl>
      get copyWith => throw _privateConstructorUsedError;
}
