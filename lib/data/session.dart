import 'package:uuid/uuid.dart';

import 'monitoring_entries.dart';

class MonitoringSession{
  final DateTime start;
  DateTime? end;
  final String id;
  final List<MonitoringEntry> entries = [];

  MonitoringSession() : start = DateTime.now(), id = const Uuid().v1();
}