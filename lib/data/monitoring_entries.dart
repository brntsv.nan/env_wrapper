import 'package:env_wrapper/helpers/log.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'monitoring_entries.freezed.dart';
part 'monitoring_entries.g.dart';

enum BlocEventType {
  create,
  event,
  stateChange,
}

enum StarageOperationType {
  write,
  read,
}

enum MonitorEventKind { bloc, network, log, storage, exception }

@freezed
class StackFrame with _$StackFrame {
  factory StackFrame({
    required String? funcName,
    required int? line,
    required int? column,
    required String path,
  }) = _StackFrameCtor;

  factory StackFrame.fromJson(Map<String, dynamic> json) =>
      _$StackFrameFromJson(json);
}

@unfreezed
class MonitoringEntry with _$MonitoringEntry {
  const MonitoringEntry._();

  factory MonitoringEntry.networkCall({
    required Uri uri,
    required String id,
    required DateTime logTimestamp,
    required DateTime start,
    dynamic request,
    DateTime? end,
    bool? isErr,
    required Map<String, dynamic> requestQuery,
    required Map<String, dynamic> requestHeaders,
    Map<String, String>? responseHeaders,
    dynamic response,
  }) = MonitoringEntryNetworkCall;

  factory MonitoringEntry.storageOperation({
    required StarageOperationType storage,
    required String storageName,
    required String key,
    required String value,
    required DateTime logTimestamp,
  }) = MonitoringEntryStarageOperation;

  factory MonitoringEntry.exception({
    required Object object,
    required List<StackFrame> frames,
  }) = MonitoringEntryException;

  factory MonitoringEntry.textLog({
    required String text,
    required EagleLogSeverity severity,
    required String id,
    required DateTime logTimestamp,
  }) = MonitoringEntryTextLog;

  factory MonitoringEntry.blocEvent({
    required String sourceBlocType,
    required BlocEventType type,
    String? eventType,
    required DateTime logTimestamp,
  }) = MonitoringBlocEventLog;

  MonitorEventKind get kind => map(
        exception: (value) => MonitorEventKind.exception,
        storageOperation: (value) => MonitorEventKind.storage,
        networkCall: (value) => MonitorEventKind.network,
        textLog: (value) => MonitorEventKind.log,
        blocEvent: (value) => MonitorEventKind.bloc,
      );

  factory MonitoringEntry.fromJson(Map<String, dynamic> json) =>
      _$MonitoringEntryFromJson(json);
}
