// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'monitoring_entries.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$StackFrameCtorImpl _$$StackFrameCtorImplFromJson(Map<String, dynamic> json) =>
    _$StackFrameCtorImpl(
      funcName: json['funcName'] as String?,
      line: (json['line'] as num?)?.toInt(),
      column: (json['column'] as num?)?.toInt(),
      path: json['path'] as String,
    );

Map<String, dynamic> _$$StackFrameCtorImplToJson(
        _$StackFrameCtorImpl instance) =>
    <String, dynamic>{
      'funcName': instance.funcName,
      'line': instance.line,
      'column': instance.column,
      'path': instance.path,
    };

_$MonitoringEntryNetworkCallImpl _$$MonitoringEntryNetworkCallImplFromJson(
        Map<String, dynamic> json) =>
    _$MonitoringEntryNetworkCallImpl(
      uri: Uri.parse(json['uri'] as String),
      id: json['id'] as String,
      logTimestamp: DateTime.parse(json['logTimestamp'] as String),
      start: DateTime.parse(json['start'] as String),
      request: json['request'],
      end: json['end'] == null ? null : DateTime.parse(json['end'] as String),
      isErr: json['isErr'] as bool?,
      requestQuery: json['requestQuery'] as Map<String, dynamic>,
      requestHeaders: json['requestHeaders'] as Map<String, dynamic>,
      responseHeaders: (json['responseHeaders'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(k, e as String),
      ),
      response: json['response'],
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$MonitoringEntryNetworkCallImplToJson(
        _$MonitoringEntryNetworkCallImpl instance) =>
    <String, dynamic>{
      'uri': instance.uri.toString(),
      'id': instance.id,
      'logTimestamp': instance.logTimestamp.toIso8601String(),
      'start': instance.start.toIso8601String(),
      'request': instance.request,
      'end': instance.end?.toIso8601String(),
      'isErr': instance.isErr,
      'requestQuery': instance.requestQuery,
      'requestHeaders': instance.requestHeaders,
      'responseHeaders': instance.responseHeaders,
      'response': instance.response,
      'runtimeType': instance.$type,
    };

_$MonitoringEntryStarageOperationImpl
    _$$MonitoringEntryStarageOperationImplFromJson(Map<String, dynamic> json) =>
        _$MonitoringEntryStarageOperationImpl(
          storage: $enumDecode(_$StarageOperationTypeEnumMap, json['storage']),
          storageName: json['storageName'] as String,
          key: json['key'] as String,
          value: json['value'] as String,
          logTimestamp: DateTime.parse(json['logTimestamp'] as String),
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$MonitoringEntryStarageOperationImplToJson(
        _$MonitoringEntryStarageOperationImpl instance) =>
    <String, dynamic>{
      'storage': _$StarageOperationTypeEnumMap[instance.storage]!,
      'storageName': instance.storageName,
      'key': instance.key,
      'value': instance.value,
      'logTimestamp': instance.logTimestamp.toIso8601String(),
      'runtimeType': instance.$type,
    };

const _$StarageOperationTypeEnumMap = {
  StarageOperationType.write: 'write',
  StarageOperationType.read: 'read',
};

_$MonitoringEntryExceptionImpl _$$MonitoringEntryExceptionImplFromJson(
        Map<String, dynamic> json) =>
    _$MonitoringEntryExceptionImpl(
      object: json['object'] as Object,
      frames: (json['frames'] as List<dynamic>)
          .map((e) => StackFrame.fromJson(e as Map<String, dynamic>))
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$MonitoringEntryExceptionImplToJson(
        _$MonitoringEntryExceptionImpl instance) =>
    <String, dynamic>{
      'object': instance.object,
      'frames': instance.frames,
      'runtimeType': instance.$type,
    };

_$MonitoringEntryTextLogImpl _$$MonitoringEntryTextLogImplFromJson(
        Map<String, dynamic> json) =>
    _$MonitoringEntryTextLogImpl(
      text: json['text'] as String,
      severity: $enumDecode(_$EagleLogSeverityEnumMap, json['severity']),
      id: json['id'] as String,
      logTimestamp: DateTime.parse(json['logTimestamp'] as String),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$MonitoringEntryTextLogImplToJson(
        _$MonitoringEntryTextLogImpl instance) =>
    <String, dynamic>{
      'text': instance.text,
      'severity': _$EagleLogSeverityEnumMap[instance.severity]!,
      'id': instance.id,
      'logTimestamp': instance.logTimestamp.toIso8601String(),
      'runtimeType': instance.$type,
    };

const _$EagleLogSeverityEnumMap = {
  EagleLogSeverity.info: 'info',
};

_$MonitoringBlocEventLogImpl _$$MonitoringBlocEventLogImplFromJson(
        Map<String, dynamic> json) =>
    _$MonitoringBlocEventLogImpl(
      sourceBlocType: json['sourceBlocType'] as String,
      type: $enumDecode(_$BlocEventTypeEnumMap, json['type']),
      eventType: json['eventType'] as String?,
      logTimestamp: DateTime.parse(json['logTimestamp'] as String),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$MonitoringBlocEventLogImplToJson(
        _$MonitoringBlocEventLogImpl instance) =>
    <String, dynamic>{
      'sourceBlocType': instance.sourceBlocType,
      'type': _$BlocEventTypeEnumMap[instance.type]!,
      'eventType': instance.eventType,
      'logTimestamp': instance.logTimestamp.toIso8601String(),
      'runtimeType': instance.$type,
    };

const _$BlocEventTypeEnumMap = {
  BlocEventType.create: 'create',
  BlocEventType.event: 'event',
  BlocEventType.stateChange: 'stateChange',
};
