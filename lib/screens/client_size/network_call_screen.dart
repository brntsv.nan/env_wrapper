import 'package:flutter/material.dart';

import '../../data/monitoring_entries.dart';

class NetworkCallScreen extends StatelessWidget {
  const NetworkCallScreen({
    super.key,
    required this.call,
  });

  final MonitoringEntryNetworkCall call;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Сетевой вызов"),
      ),
      body: SafeArea(
        child: ListView(
          padding: const EdgeInsets.all(10),
          children: [
            renderHeader(context, "Full url"),
            const SizedBox(
              height: 10,
            ),
            Text(
              call.uri.toString(),
              style: Theme.of(context).textTheme.titleSmall,
            ),
            const SizedBox(
              height: 10,
            ),
            renderHeader(context, "Request Query"),
            const SizedBox(
              height: 10,
            ),
            for (final rH in call.requestQuery.entries) ...[
              Row(
                children: [
                  SizedBox(
                      width: 120,
                      child: Text(
                        rH.key,
                        style: Theme.of(context).textTheme.titleSmall,
                      )),
                  Expanded(
                      child: Text(rH.value.toString(),
                          style: Theme.of(context).textTheme.titleSmall)),
                ],
              ),
              const Divider(
                height: 1,
              )
            ],
            const SizedBox(
              height: 20,
            ),
            renderHeader(context, "Request headers"),
            const SizedBox(
              height: 10,
            ),
            for (final rH in call.requestHeaders.entries) ...[
              Row(
                children: [
                  SizedBox(
                      width: 120,
                      child: Text(
                        rH.key,
                        style: Theme.of(context).textTheme.titleSmall,
                      )),
                  Expanded(
                      child: Text(rH.value.toString(),
                          style: Theme.of(context).textTheme.titleSmall)),
                ],
              ),
              const Divider(
                height: 1,
              )
            ],
            renderHeader(context, "Request"),
            const SizedBox(
              height: 20,
            ),
            MapRenderer(data: call.request),
            const SizedBox(
              height: 20,
            ),
            renderHeader(context, "Response headers"),
            const SizedBox(
              height: 10,
            ),
            for (final rH in call.responseHeaders?.entries.toList() ?? []) ...[
              Row(
                children: [
                  SizedBox(
                      width: 120,
                      child: Text(
                        rH.key,
                        style: Theme.of(context).textTheme.titleSmall,
                      )),
                  Expanded(
                      child: Text(rH.value.toString(),
                          style: Theme.of(context).textTheme.titleSmall)),
                ],
              ),
              const Divider(
                height: 1,
              ),
            ],
            const SizedBox(
              height: 10,
            ),
            renderHeader(context, "Response"),
            const SizedBox(
              height: 20,
            ),
            MapRenderer(data: call.response),
          ],
        ),
      ),
    );
  }

  renderHeader(BuildContext context, String title) {
    return Text(
      title,
      style:
          Theme.of(context).textTheme.titleLarge!.copyWith(color: Colors.green),
    );
  }
}

class MapRenderer extends StatefulWidget {
  const MapRenderer({super.key, required this.data});

  final dynamic data;

  @override
  State<MapRenderer> createState() => _MapRendererState();
}

class _MapRendererState extends State<MapRenderer> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    bool isMapOrArray =
        widget.data is Map<String, dynamic> || widget.data is List<dynamic>;

    if (isMapOrArray && !isExpanded) {
      return Row(
        children: [
          IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              setState(() {
                isExpanded = true;
              });
            },
            icon: const Icon(Icons.play_arrow),
          ),
          const Spacer(),
        ],
      );
    }

    if (widget.data is Map<String, dynamic>) {
      return Container(
        padding: const EdgeInsets.only(left: 8),
        decoration: const BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.grey, width: 1),
            top: BorderSide(color: Colors.grey, width: 1),
            bottom: BorderSide(color: Colors.grey, width: 1),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            for (final k in widget.data.keys)
              if (widget.data[k] is Map<String, dynamic> ||
                  widget.data[k] is List<dynamic>)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      "$k: ",
                      style: Theme.of(context)
                          .textTheme
                          .titleSmall!
                          .copyWith(color: Colors.blue),
                    ),
                    MapRenderer(data: widget.data[k]),
                  ],
                )
              else
                Row(
                  children: [
                    Text(
                      "$k: ",
                      style: Theme.of(context)
                          .textTheme
                          .titleSmall!
                          .copyWith(color: Colors.blueGrey),
                    ),
                    Expanded(child: MapRenderer(data: widget.data[k])),
                  ],
                )
          ],
        ),
      );
    } else if (widget.data is List<dynamic>) {
      return Container(
        padding: const EdgeInsets.only(left: 8, top: 4, bottom: 4),
        decoration: const BoxDecoration(
            border: Border(
          left: BorderSide(color: Colors.orange, width: 1),
          top: BorderSide(color: Colors.orange, width: 1),
          bottom: BorderSide(color: Colors.orange, width: 1),
        )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            for (int i = 0; i < widget.data.length; i++)
              Row(
                children: [
                  Text("$i "),
                  Expanded(
                    child: MapRenderer(
                      data: widget.data[i],
                    ),
                  ),
                ],
              )
          ],
        ),
      );
    } else {
      return Text(widget.data.toString());
    }
  }
}
