import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:package_info_plus/package_info_plus.dart';

import 'package:env_wrapper/core/options.dart';
import 'package:env_wrapper/data/session.dart';
import 'package:env_wrapper/observers/screen_recording.dart';
import 'package:env_wrapper/screens/client_size/memory_management.dart';
import 'package:env_wrapper/services/log_vault.dart';
import 'package:env_wrapper/services/session_vault.dart';

import '../../core/guard.dart';
import '../../widgets/env_editor/env_editor.dart';
import '../../widgets/recorder_blink.dart';
import 'monitoring_screen.dart';

typedef DevToolsLayerBuilder = Widget Function({
  required Widget child,
});

typedef Launcher<TEnv extends IApplicationEnvironment> = FutureOr Function(
    TEnv env, DevToolsLayerBuilder qaToolsLayer);

typedef OnReloadCallback = FutureOr Function();

class DevToolsScreen<TEnv extends IApplicationEnvironment>
    extends StatefulWidget {
  const DevToolsScreen({
    super.key,
    required this.options,
    required this.launcher,
    required this.onRestart,
  });

  final ApplicationOptions<TEnv> options;
  final Launcher<TEnv> launcher;
  final OnReloadCallback? onRestart;

  @override
  State<DevToolsScreen<TEnv>> createState() => _DevToolsScreenState<TEnv>();
}

class _DevToolsScreenState<TEnv extends IApplicationEnvironment>
    extends State<DevToolsScreen<TEnv>> with TickerProviderStateMixin {
  late TabController tabController;

  late SharedPreferencesStorage prefs;

  static int selectedEnvIndex = 0;

  bool isLaunching = false;

  bool isParamsExpanded = false;

  List<TEnv> envs = [];

  static String envIndexKey = "qa_tools_env_index_history";

  Future restoreLastIndex() async {
    prefs = SharedPreferencesStorage();
    await prefs.init();
    final index = (await prefs.getValue(envIndexKey) ?? 0) as int;
    setState(() {
      selectedEnvIndex = index;
      tabController.animateTo(index);
    });
  }

  @override
  void initState() {
    envs = widget.options.environments;
    tabController = TabController(
        length: widget.options.environments.length,
        vsync: this,
        initialIndex: selectedEnvIndex);

    tabController.addListener(() {
      setState(() {
        selectedEnvIndex = tabController.index;
        prefs.setIntValue(
          envIndexKey,
          selectedEnvIndex,
        );
      });
    });
    restoreLastIndex();

    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.from(
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.blue,
          brightness: Brightness.dark,
        ),
        useMaterial3: true,
      ),
      home: Builder(builder: (context) {
        Future.microtask(
          () {
            if (QaToolsLayer.eventToOpen != null) {
              openLogEntry(context, QaToolsLayer.eventToOpen!);
              QaToolsLayer.eventToOpen = null;
            }
          },
        );

        Future.microtask(
          () {
            if (QaToolsLayer.sessionToOpen != null) {
              final session = QaToolsLayer.sessionToOpen!;
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => MonitoringScreen(entry: session),
                ),
              );
              QaToolsLayer.sessionToOpen = null;
            }
          },
        );

        return GestureDetector(
          onPanUpdate: (details) {
            if (details.delta.dx > 20) {
              QaToolsLayer.visibility.value = false;
            }
          },
          child: Scaffold(
            appBar: buildAppBar(context),
            body: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(
                    height: 4,
                  ),
                  const RecorderBlock(),
                  Flexible(
                    flex: isParamsExpanded ? 1 : 0,
                    child: buildParamsTabBlock(),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const MemoryManagementScreen(),
                        ),
                      );
                    },
                    child: const Text("Просмотр памяти"),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Row(
        children: [
          if (QaToolsLayer.visibility.value)
            IconButton(
              onPressed: () {
                setState(() {
                  QaToolsLayer.visibility.value = false;
                });
              },
              icon: const Icon(
                Icons.close,
              ),
            ),
          SvgPicture.network(
            "https://nanagency.ru/themes/nan/images/logo.svg",
            height: 20,
          ),
          const Spacer(),
          FutureBuilder<PackageInfo>(
            future: PackageInfo.fromPlatform(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return const Center();
              final info = snapshot.data!;
              return Text(
                "${info.appName} \n ${info.version} - ${info.buildNumber}",
                textAlign: TextAlign.end,
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .copyWith(fontWeight: FontWeight.w700),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget buildParamsTabBlock() {
    var mondatoryTabButtons = [
      buildToggleTabVisibleBtn(),
      const Divider(height: 2),
      startBtn(selectedEnvIndex),
    ];
    var content = isParamsExpanded
        ? TabBarView(
            controller: tabController,
            children: [
              for (int i = 0; i < envs.length; i++)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: ListView(
                        children: [
                          EnvEditor(
                            env: envs[i],
                            onEnvChanged: (value) {
                              envs[i] = value;
                              setState(() {});
                            },
                          ),
                        ],
                      ),
                    ),
                    ...mondatoryTabButtons,
                  ],
                ),
            ],
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: mondatoryTabButtons,
          );
    return Card(
      child: Column(
        mainAxisSize: isParamsExpanded ? MainAxisSize.max : MainAxisSize.min,
        children: [
          TabBar(
            controller: tabController,
            tabs: [
              for (final env in widget.options.environments)
                Tab(
                  text: env.envTitle,
                ),
            ],
          ),
          Flexible(
            flex: isParamsExpanded ? 1 : 0,
            child: content,
          )
        ],
      ),
    );
  }

  Widget buildToggleTabVisibleBtn() {
    return TextButton(
      onPressed: () {
        setState(() {
          isParamsExpanded = !isParamsExpanded;
        });
      },
      child: Text(
        isParamsExpanded ? "Скрыть параметры" : "Открыть параметры",
      ),
    );
  }

  Widget startBtn(int i) {
    return TextButton(
      style: ButtonStyle(
          foregroundColor: WidgetStateProperty.all(Colors.orangeAccent)),
      onPressed: () async {
        await startEnv(i);
      },
      child: Text(
          "Запустить конфиг ${envs[i].envTitle} ${isLaunching ? 'Запуск' : ''}"),
    );
  }

  Future<void> startEnv(int i) async {
    setState(() {
      isLaunching = true;
    });

    if (widget.onRestart != null) {
      await widget.onRestart!();
    }

    GetIt.I.get<SessionVault>().appRestarted;

    final newKey = GlobalKey();

    builder({
      required Widget child,
    }) {
      return QaToolsLayer<TEnv>(
        key: newKey,
        options: widget.options,
        launcher: widget.launcher,
        onRestart: widget.onRestart,
        child: child,
      );
    }

    QaToolsLayer.visibility.value = false;

    FlutterError.onError = (details) {
      GetIt.I.get<LogVault>().addException(details.exception, details.stack!);
    };

    runZonedGuarded(
      () async => await widget.launcher(envs[i], builder)!,
      (error, stack) {
        GetIt.I.get<LogVault>().addException(error, stack);
      },
    );
    setState(() {
      isLaunching = false;
    });
  }
}

class RecorderBlock extends StatefulWidget {
  const RecorderBlock({
    super.key,
  });

  @override
  State<RecorderBlock> createState() => _RecorderBlockState();
}

class _RecorderBlockState extends State<RecorderBlock> {
  double processingState = 0.0;
  bool isProcessing = false;

  void listener() {
    setState(() {});
  }

  @override
  void initState() {
    GetIt.I.get<SessionVault>().addListener(
          listener,
        );
    super.initState();
  }

  @override
  void dispose() {
    GetIt.I.get<SessionVault>().removeListener(
          listener,
        );
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final vault = GetIt.I.get<SessionVault>();

    return SizedBox(
      height: 300,
      child: Card(
        child: Row(
          children: [
            buildToolbar(),
            const VerticalDivider(),
            Expanded(
              child: ListView(
                padding: const EdgeInsets.all(8),
                children: [
                  for (final session in [
                    ...vault.sessions,
                    if (vault.currentSession != null) vault.currentSession!,
                  ].reversed)
                    RecordingCard(key: ValueKey(session.id), session: session),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildToolbar() {
    return SizedBox(
      width: 50,
      child: Column(
        children: [
          const SizedBox(height: 12),
          Text(
            "REC",
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  color: Colors.red,
                ),
          ),
          IconButton(
            onPressed: () {
              GetIt.I.get<SessionVault>().startRecording();
              //QaToolsRecorder.isScreenRecording.value = true;
            },
            icon: const Icon(Icons.upload),
          ),
          ValueListenableBuilder<bool>(
            valueListenable: QaToolsRecorder.isScreenRecording,
            builder: (context, value, child) => Row(
              children: [
                if (!value)
                  IconButton(
                    onPressed: () {
                      GetIt.I.get<SessionVault>().startRecording();
                      //QaToolsRecorder.isScreenRecording.value = true;
                    },
                    icon: const Icon(Icons.circle, color: Colors.red),
                  )
                /*else
                  IconButton(
                    onPressed: () {
                      QaToolsRecorder.isScreenRecording.value = false;
                    },
                    icon: Icon(Icons.stop),
                  ),*/
              ],
            ),
          ),
          ValueListenableBuilder<bool>(
            valueListenable: QaToolsRecorder.isProcessing,
            builder: (context, isProcessing, child) {
              if (isProcessing) {
                return ValueListenableBuilder<double>(
                  valueListenable: QaToolsRecorder.processingState,
                  builder: (context, progress, child) =>
                      CircularProgressIndicator(
                    value: progress,
                  ),
                );
              } else {
                return const Center();
              }
            },
          ),
        ],
      ),
    );
  }
}

class RecordingCard extends StatelessWidget {
  const RecordingCard({
    super.key,
    required this.session,
  });

  final MonitoringSession session;

  @override
  Widget build(BuildContext context) {
    final dtFormatter = DateFormat.Hms();
    return Card(
      color: Theme.of(context).colorScheme.secondary,
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => MonitoringScreen(entry: session),
            ),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                children: [
                  if (session.end == null) ...[
                    const RecordingBlink(),
                    const SizedBox(
                      width: 8,
                    ),
                  ],
                  Expanded(
                    child: Text(
                      "Запись с ${dtFormatter.format(session.start)} записей ${session.entries.length}",
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: Theme.of(context).colorScheme.onSecondary),
                    ),
                  ),
                ],
              ),
              Text(
                session.id,
                style: Theme.of(context)
                    .textTheme
                    .labelMedium!
                    .copyWith(color: Theme.of(context).colorScheme.onSecondary),
              )
            ],
          ),
        ),
      ),
    );
  }
}
