import 'package:env_wrapper/data/monitoring_entries.dart';
import 'package:flutter/material.dart';

class ExceptionScreen extends StatelessWidget {
  const ExceptionScreen({
    super.key,
    required this.ex,
  });

  final MonitoringEntryException ex;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Exception"),
      ),
      body: ListView(
        padding: const EdgeInsets.all(8),
        children: [
          Text(
            ex.object.toString(),
            style: Theme.of(context)
                .textTheme
                .labelMedium!
                .copyWith(color: Colors.redAccent),
          ),
          const SizedBox(height: 12),
          const Text("Trace"),
          const SizedBox(height: 12),
          for (final frame in ex.frames)
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  children: [
                    Expanded(child: Text(frame.funcName ?? "")),
                    Text("R(${frame.line}):C(${frame.column})",
                        style: Theme.of(context)
                            .textTheme
                            .labelMedium!
                            .copyWith(color: Colors.white.withOpacity(0.5)))
                  ],
                ),
                Text(
                  frame.path,
                  style: Theme.of(context)
                      .textTheme
                      .labelMedium!
                      .copyWith(color: Colors.white.withOpacity(0.5)),
                ),
                const Divider(),
              ],
            ),
          Container(
            decoration: const BoxDecoration(
              border: Border.symmetric(
                vertical: BorderSide(color: Colors.white, width: 1),
              ),
            ),
          )
        ],
      ),
    );
  }
}
