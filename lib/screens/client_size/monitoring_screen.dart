import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:env_wrapper/data/monitoring_entries.dart';
import 'package:env_wrapper/data/session.dart';
import 'package:env_wrapper/screens/client_size/exception_screen.dart';

import '../../services/log_vault.dart';
import 'network_call_screen.dart';

class MonitoringScreen extends StatefulWidget {
  const MonitoringScreen({super.key, required this.entry});

  final MonitoringSession entry;

  @override
  State<MonitoringScreen> createState() => _MonitoringScreenState();
}

class _MonitoringScreenState extends State<MonitoringScreen> {
  final String searchQuery = "";
  final Map<MonitorEventKind, String> types = {
    MonitorEventKind.bloc: "Bloc",
    MonitorEventKind.network: "Сеть",
    MonitorEventKind.log: "Текст",
    MonitorEventKind.exception: "Ошибки",
    MonitorEventKind.storage: "Хранилище",
  };

  late List<MonitorEventKind> selectedTypes = types.keys.toList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.entry.id.substring(0, 5)),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(150),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: TextField(
                  decoration: InputDecoration(
                    isDense: true,
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Wrap(
                runAlignment: WrapAlignment.start,
                children: [
                  for (final t in types.entries) ...[
                    ChoiceChip(
                      label: Text(t.value),
                      onSelected: (value) {
                        if (value) {
                          selectedTypes.add(t.key);
                        } else {
                          selectedTypes.removeWhere(
                            (element) => element.toString() == t.key.toString(),
                          );
                        }
                        setState(() {});
                      },
                      selected: selectedTypes.contains(t.key),
                    ),
                    const SizedBox(width: 12),
                  ]
                ],
              )
            ],
          ),
        ),
        actions: [
          IconButton(
              onPressed: () {
                LogVault.entries.clear();
              },
              icon: const Icon(Icons.refresh))
        ],
      ),
      body: ListView(
        children: [
          for (final l
              in widget.entry.entries.where(filterEntries).toList().reversed)
            l.map(
              exception: (value) => ExceptionEventRenderer(
                ex: value,
              ),
              storageOperation: (value) => const Center(
                child: Text("storage"),
              ),
              networkCall: (value) => NetworkCallRenderer(
                call: value,
              ),
              textLog: (value) => TextEntryRenderer(log: value),
              blocEvent: (value) => BlocEventRenderer(event: value),
            )
        ],
      ),
    );
  }

  bool filterEntries(MonitoringEntry element) =>
      selectedTypes.contains(element.kind);
}

extension LogPrint<T> on T {
  T logPrevious() {
    return this;
  }
}

class MonitoringEventIcon extends StatelessWidget {
  const MonitoringEventIcon({
    super.key,
    required this.entry,
  });

  final MonitoringEntry entry;

  @override
  Widget build(BuildContext context) {
    return entry.map(
      networkCall: (value) => const Icon(
        Icons.network_ping,
        size: 16,
        color: Colors.white,
      ),
      storageOperation: (value) => const Icon(
        Icons.storage,
        size: 16,
        color: Colors.white,
      ),
      exception: (value) => const Icon(
        Icons.error,
        color: Colors.red,
        size: 20,
      ),
      textLog: (value) => const Icon(
        Icons.text_fields,
        size: 20,
        color: Colors.white,
      ),
      blocEvent: (value) => Image.network(
        "https://plugins.jetbrains.com/files/12129/261752/icon/pluginIcon.png",
        height: 20,
      ),
    );
  }
}

openLogEntry(BuildContext context, MonitoringEntry entry) {
  entry.map(
    networkCall: (value) => Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => NetworkCallScreen(call: value),
    )),
    storageOperation: (value) {},
    exception: (value) => Navigator.push(context, MaterialPageRoute(
      builder: (context) {
        return ExceptionScreen(ex: value);
      },
    )),
    textLog: (value) {},
    blocEvent: (value) {},
  );
}

class ExceptionEventRenderer extends StatelessWidget {
  const ExceptionEventRenderer({
    super.key,
    required this.ex,
  });

  final MonitoringEntryException ex;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        openLogEntry(context, ex);
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              MonitoringEventIcon(entry: ex),
              const SizedBox(width: 12),
              Expanded(
                child: Text(
                  ex.object.toString(),
                  maxLines: 2,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class BlocEventRenderer extends StatelessWidget {
  const BlocEventRenderer({
    super.key,
    required this.event,
  });

  final MonitoringBlocEventLog event;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                MonitoringEventIcon(entry: event),
                const SizedBox(
                  width: 12,
                ),
                Text(event.sourceBlocType),
              ],
            ),
            if (event.type == BlocEventType.create) const Text("Created"),
            if (event.type == BlocEventType.stateChange)
              Text("State -> ${event.eventType}"),
            if (event.type == BlocEventType.event)
              Text("Event - ${event.eventType}"),
          ],
        ),
      ),
    );
  }
}

class NetworkCallRenderer extends StatelessWidget {
  const NetworkCallRenderer({super.key, required this.call});

  final MonitoringEntryNetworkCall call;

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.hardEdge,
      child: InkWell(
        onTap: () {
          openLogEntry(context, call);
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                children: [
                  MonitoringEventIcon(entry: call),
                  const SizedBox(width: 12),
                  if (call.isErr ?? false) ...[
                    const Icon(
                      Icons.error,
                      size: 16,
                      color: Colors.redAccent,
                    ),
                    const SizedBox(width: 12),
                  ],
                  Expanded(
                    child: Opacity(
                        opacity: 0.5,
                        child: Text(
                          call.uri.host,
                          maxLines: 1,
                          softWrap: false,
                          style: Theme.of(context).textTheme.bodyMedium,
                        )),
                  ),
                  const Spacer(),
                  Text(DateFormat("HH:mm:ss.S").format(call.start)),
                ],
              ),
              Text(
                call.uri.path,
                style: Theme.of(context).textTheme.titleSmall,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class TextEntryRenderer extends StatelessWidget {
  const TextEntryRenderer({
    super.key,
    required this.log,
  });
  final MonitoringEntryTextLog log;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                Text(
                  "Текст ${log.severity.name}",
                  style: Theme.of(context).textTheme.bodyMedium,
                )
              ],
            ),
            Text(
              log.text,
              style: Theme.of(context).textTheme.titleSmall,
            )
          ],
        ),
      ),
    );
  }
}
