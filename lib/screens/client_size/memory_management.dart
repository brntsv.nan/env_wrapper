import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class KeychainStorage extends IStorageProvider {
  late FlutterSecureStorage secure;

  @override
  Future clear() {
    return secure.deleteAll();
  }

  @override
  Future<List<String>> getKeys() async {
    return (await secure.readAll()).keys.toList();
  }

  @override
  Future<Object?> getValue(String key) async {
    return await secure.read(key: key);
  }

  @override
  Future init() async {
    secure = const FlutterSecureStorage();
  }

  @override
  Future deleteKey(String key) async {
    await secure.delete(key: key);
  }

  @override
  final String title = "Keychain";
}

class SharedPreferencesStorage extends IStorageProvider {
  late SharedPreferences prefs;

  @override
  Future clear() {
    return prefs.clear();
  }

  @override
  Future<List<String>> getKeys() async {
    return prefs.getKeys().toList();
  }

  @override
  Future<Object?> getValue(String key) async {
    return prefs.get(key);
  }

  Future setStringValue(String key, String value) async {
    return prefs.setString(key, value);
  }

  Future setIntValue(String key, int value) async {
    return prefs.setInt(key, value);
  }

  @override
  Future init() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  final String title = "SharedPrefs";

  @override
  Future deleteKey(String key) async {
    await prefs.remove(key);
  }
}

abstract class IStorageProvider {
  abstract final String title;

  Future init();

  Future<List<String>> getKeys();

  Future<Object?> getValue(String key);

  Future deleteKey(String key);

  Future clear();
}

class MemoryManagementScreen extends StatefulWidget {
  const MemoryManagementScreen({super.key});

  @override
  State<MemoryManagementScreen> createState() => _MemoryManagementScreenState();
}

class _MemoryManagementScreenState extends State<MemoryManagementScreen>
    with TickerProviderStateMixin {
  @override
  void initState() {
    initStorages();
    super.initState();
  }

  late TabController tabController;

  List<IStorageProvider> storages = [
    SharedPreferencesStorage(),
    KeychainStorage(),
  ];

  initStorages() async {
    tabController = TabController(length: storages.length, vsync: this);
    for (final provider in storages) {
      await provider.init();
    }
    refreshData();
  }

  refreshData() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text("Управление памятью"),
        actions: [
          IconButton(
            onPressed: () async {
              for (final provider in storages) {
                await provider.clear();
              }
              refreshData();
            },
            icon: const Icon(Icons.delete),
          ),
        ],
        bottom: TabBar(
          controller: tabController,
          tabs: [
            for (final storage in storages)
              Tab(
                text: storage.title,
              )
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          for (final storage in storages)
            FutureBuilder<List<String>>(
                future: storage.getKeys(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return const Center();
                  final keys = snapshot.data!;
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListView(
                      children: [
                        if (keys.isEmpty)
                          const Text(
                            "Записей в памяти нет",
                            textAlign: TextAlign.center,
                          ),
                        for (final key in keys) ...[
                          FutureBuilder(
                            future: storage.getValue(key),
                            builder: (context, snapshot) {
                              if (!snapshot.hasData) return const Center();
                              return Card(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: DataRow(
                                    title: key,
                                    storage: storage,
                                    data: snapshot.data!,
                                    onChange: () {
                                      refreshData();
                                    },
                                  ),
                                ),
                              );
                            },
                          ),
                        ]
                      ],
                    ),
                  );
                })
        ],
      ),
    );
  }
}

class DataRow extends StatelessWidget {
  const DataRow({
    super.key,
    required this.title,
    required this.data,
    required this.storage,
    required this.onChange,
  });

  final String title;
  final Object data;
  final IStorageProvider storage;
  final VoidCallback onChange;

  String formatJson(String jsonString, {int indentation = 2}) {
    try {
      final jsonObject = json.decode(jsonString);
      final encoder = JsonEncoder.withIndent(' ' * indentation);
      return encoder.convert(jsonObject);
    } catch (ex) {
      return jsonString;
    }
  }

  @override
  Widget build(BuildContext context) {
    final text = formatJson(data.toString());
    final linesCount = text.split("\n");
    return ExpandablePanel(
      header: Row(
        children: [
          Expanded(
            child: Text(
              title,
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(fontWeight: FontWeight.w700),
            ),
          ),
          Chip(label: Text(data.runtimeType.toString())),
        ],
      ),
      collapsed: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            text.isEmpty ? "[Пустая строка]" : text,
            maxLines: 2,
          ),
          if (linesCount.length > 2)
            Text(
              "еще ${linesCount.length - 2} строк",
              style: Theme.of(context)
                  .textTheme
                  .bodyLarge!
                  .copyWith(fontStyle: FontStyle.italic),
            ),
        ],
      ),
      expanded: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            text,
          ),
          Row(
            children: [
              IconButton(
                onPressed: () async {
                  await Clipboard.setData(ClipboardData(text: data.toString()));
                },
                icon: const Icon(Icons.copy),
              ),
              IconButton(
                onPressed: () {
                  storage.deleteKey(title);
                  onChange();
                },
                icon: const Icon(Icons.delete_outline),
              )
            ],
          )
        ],
      ),
    );
  }
}
