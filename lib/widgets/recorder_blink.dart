import 'package:flutter/material.dart';

class RecordingBlink extends StatefulWidget {
  const RecordingBlink({
    super.key,
  });

  @override
  State<RecordingBlink> createState() => _RecordingBlinkState();
}

class _RecordingBlinkState extends State<RecordingBlink>
    with TickerProviderStateMixin {
  late AnimationController controller = AnimationController(
      vsync: this,
      lowerBound: 0.5,
      upperBound: 1,
      duration: const Duration(
        milliseconds: 800,
      ),
      reverseDuration: const Duration(
        milliseconds: 800,
      ));

  @override
  void initState() {
    controller.repeat(reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) => Opacity(
        opacity: controller.value,
        child: const Icon(
          Icons.circle,
          color: Colors.red,
          size: 12,
        ),
      ),
    );
  }
}
