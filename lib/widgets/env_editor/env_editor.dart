import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../core/options.dart';

class EnvEditor<TEnv extends IApplicationEnvironment> extends StatefulWidget {
  const EnvEditor({
    super.key,
    required this.env,
    required this.onEnvChanged,
  });

  final TEnv env;

  final ValueChanged<TEnv> onEnvChanged;

  @override
  State<EnvEditor<TEnv>> createState() => _EnvEditorState();
}

class _EnvEditorState<TEnv extends IApplicationEnvironment>
    extends State<EnvEditor<TEnv>> {
  @override
  Widget build(BuildContext context) {
    final json = widget.env.toJson();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          for (final key in json.keys) ...[
            Row(
              children: [
                SizedBox(
                  width: 140,
                  child: Text(
                    key,
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(fontWeight: FontWeight.w700),
                  ),
                ),
                if (json[key] is String)
                  Expanded(
                    child: StringEditor(
                      onChange: (value) {
                        json[key] = value;
                        widget.onEnvChanged(widget.env.fromJson(json) as TEnv);
                      },
                      initialData: json[key],
                    ),
                  )
                else
                  Text(json[key].toString())
              ],
            ),
            const Divider()
          ]
        ],
      ),
    );
  }
}

class StringEditor extends StatefulWidget {
  const StringEditor({
    super.key,
    required this.initialData,
    required this.onChange,
  });
  final String initialData;
  final ValueChanged<String> onChange;

  @override
  State<StringEditor> createState() => _StringEditorState();
}

class _StringEditorState extends State<StringEditor> {
  late TextEditingController controller;

  @override
  void initState() {
    controller = TextEditingController(text: widget.initialData);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TextField(
            decoration: const InputDecoration(border: InputBorder.none),
            maxLines: null,
            controller: controller,
            onChanged: (value) async {
              widget.onChange(value);
            },
          ),
        ),
        IconButton(
          onPressed: () async {
            controller.text =
                (await Clipboard.getData("text/plain"))?.text ?? '';
            widget.onChange(controller.text);
          },
          icon: const Icon(Icons.paste),
        ),
      ],
    );
  }
}
