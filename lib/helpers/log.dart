import 'dart:developer';

import 'package:get_it/get_it.dart';
import 'package:uuid/uuid.dart';

import '../data/monitoring_entries.dart';
import '../services/log_vault.dart';

enum EagleLogSeverity{
  info,
}

eagleLog(EagleLogSeverity severity, String text) {
  GetIt.I.get<LogVault>().addEntry(MonitoringEntry.textLog(id: const Uuid().v1(), text: text, severity: severity, logTimestamp: DateTime.now()));
  log("[$severity]> $text");
}
