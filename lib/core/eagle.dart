import 'package:get_it/get_it.dart';

import '../services/log_vault.dart';

class Eagle {
  static void registerBlocCreate(Object cubit) {
    GetIt.I.get<LogVault>().addBlocCreateEntry(cubit);
  }

  static void registerBlocEvent(Object bloc, Object event) {
    GetIt.I.get<LogVault>().addBlocEventEntry(bloc, event);
  }

  static void registerBlocChange(Object cubit, Object nextState) {
    GetIt.I.get<LogVault>().addBlocStateEntry(cubit, nextState);
  }
}
