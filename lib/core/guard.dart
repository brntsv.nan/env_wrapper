// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart' as intl;

import 'package:env_wrapper/services/log_vault.dart';
import 'package:env_wrapper/services/session_vault.dart';

import '../data/monitoring_entries.dart';
import '../data/session.dart';
import '../observers/screen_recording.dart';
import '../screens/client_size/devtools_screen.dart';
import 'options.dart';

launchWithoutDevtool<TEnv extends IApplicationEnvironment>(
    TEnv env, Launcher<TEnv> launcher) {
  initServices();
  launcher(
      env,
      ({
        required Widget child,
      }) =>
          child);
}

openDevtool<TEnv extends IApplicationEnvironment>(
    ApplicationOptions<TEnv> options,
    Launcher<TEnv> launcher,
    OnReloadCallback? onRestart) {
  runApp(DevToolsScreen<TEnv>(
    options: options,
    launcher: launcher,
    onRestart: onRestart,
  ));
}

class QaToolsLayer<TEnv extends IApplicationEnvironment>
    extends StatefulWidget {
  const QaToolsLayer({
    super.key,
    required this.child,
    required this.options,
    required this.launcher,
    required this.onRestart,
  });

  final Widget child;
  final ApplicationOptions<TEnv> options;
  final Launcher<TEnv> launcher;
  final OnReloadCallback? onRestart;

  static MonitoringEntry? eventToOpen;
  static MonitoringSession? sessionToOpen;

  static final ValueNotifier<bool> visibility = ValueNotifier<bool>(false);

  @override
  State<QaToolsLayer<TEnv>> createState() => _QaToolsLayerState<TEnv>();
}

class _QaToolsLayerState<TEnv extends IApplicationEnvironment>
    extends State<QaToolsLayer<TEnv>> {
  onVisibilityChanged() {
    setState(() {});
  }

  @override
  void initState() {
    QaToolsLayer.visibility.addListener(onVisibilityChanged);

    super.initState();
  }

  @override
  void dispose() {
    QaToolsLayer.visibility.removeListener(onVisibilityChanged);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: QaToolsRecorder(
        child: Stack(
          children: [
            widget.child,
            if (QaToolsLayer.visibility.value)
              DevToolsScreen<TEnv>(
                options: widget.options,
                launcher: widget.launcher,
                onRestart: widget.onRestart,
              ),
            const EagleBar(),
          ],
        ),
      ),
    );
  }
}

class EagleBar extends StatefulWidget {
  const EagleBar({
    super.key,
  });

  @override
  State<EagleBar> createState() => _EagleBarState();
}

class _EagleBarState extends State<EagleBar> {
  final vault = GetIt.I.get<SessionVault>();

  onSessionChange() {
    setState(() {});
  }

  @override
  void initState() {
    vault.addListener(onSessionChange);
    super.initState();
  }

  @override
  void dispose() {
    vault.removeListener(onSessionChange);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 0,
      top: 30,
      child: Container(
        width: 40,
        decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.5),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(12),
              bottomLeft: Radius.circular(12),
            )),
        clipBehavior: Clip.hardEdge,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Material(
              color: Colors.black,
              child: InkWell(
                onTap: () {
                  QaToolsLayer.visibility.value = true;
                },
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(12),
                    child: Text(
                      "🦅",
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: Colors.white, fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
              ),
            ),
            /*Divider(
              color: Colors.white,
            ),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                GetIt.I.get<SessionVault>().startRecording();
              },
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                  child: Container(
                    height: 14,
                    width: 14,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                QaToolsLayer.sessionToOpen = GetIt.I.get<SessionVault>().currentSession;
                QaToolsLayer.visibility.value = true;
              },
              child: Container(
                margin: EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(4),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    CurrentSessionTimeElapsed(),
                    Center(
                      child: Text(
                        GetIt.I
                                .get<SessionVault>()
                                .currentSession
                                ?.id
                                .substring(0, 5) ??
                            "---",
                        style: Theme.of(context)
                            .textTheme
                            .labelMedium!
                            .copyWith(color: Colors.orange),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.open_in_new,
                        color: Colors.white,
                        size: 16,
                      ),
                    ),
                  ],
                ),
              ),
            ),*/
            /*Expanded(
              child: ListView(
                padding: EdgeInsets.all(8),
                children: [
                  for (final entry in vault.currentSession!.entries.reversed)
                    GestureDetector(
                      onTap: () {
                        QaToolsLayer.eventToOpen = entry;
                        QaToolsLayer.visibility.value = true;
                      },
                      child: Container(
                        margin: EdgeInsets.only(bottom: 12),
                        padding: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: Colors.white.withOpacity(0.5)),
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Center(
                          child: Column(
                            children: [
                              MonitoringEventIcon(
                                entry: entry,
                              ),
                              entry.maybeMap(
                                textLog: (value) => RotatedBox(
                                  quarterTurns: 0,
                                  child: Text(
                                    value.text,
                                    textAlign: TextAlign.center,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodySmall!
                                        .copyWith(color: Colors.white, fontSize: 8),
                                  ),
                                ),
                                exception: (value) => RotatedBox(
                                  quarterTurns: 0,
                                  child: Text(
                                    value.object.toString(),
                                    textAlign: TextAlign.start,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodySmall!
                                        .copyWith(color: Colors.white, fontSize: 8),
                                  ),
                                ),
                                blocEvent: (value) => RotatedBox(
                                  quarterTurns: 0,
                                  child: Text(
                                    value.sourceBlocType.toString() +
                                        "\n -> \n" + (value.eventType?.toString()??""),
                                    textAlign: TextAlign.start,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodySmall!
                                        .copyWith(color: Colors.white, fontSize: 8),
                                  ),
                                ),
                                networkCall: (value) => RotatedBox(
                                  quarterTurns: 0,
                                  child: Text(
                                    value.uri.path,
                                    textAlign: TextAlign.start,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodySmall!
                                        .copyWith(color: (value.isErr??false)? Colors.red: Colors.white, fontSize: 8),
                                  ),
                                ),
                                orElse: () => Center(),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                ],
              ),
            ),*/
          ],
        ),
      ),
    );
  }
}

class CurrentSessionTimeElapsed extends StatefulWidget {
  const CurrentSessionTimeElapsed({
    super.key,
  });

  @override
  State<CurrentSessionTimeElapsed> createState() =>
      _CurrentSessionTimeElapsedState();
}

class _CurrentSessionTimeElapsedState extends State<CurrentSessionTimeElapsed>
    with TickerProviderStateMixin {
  late Ticker ticker;
  Duration elapsed = Duration.zero;

  void onTicker(Duration _) {
    final sm = GetIt.I.get<SessionVault>();
    final sessionStart = sm.currentSession!.start;
    setState(() {
      elapsed = DateTime.now().difference(sessionStart);
    });
  }

  @override
  void initState() {
    ticker = createTicker(onTicker);
    ticker.start();
    super.initState();
  }

  @override
  void dispose() {
    ticker.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final formatted =
        intl.DateFormat("mm:ss").format(DateTime(2000).add(elapsed));
    return Padding(
      padding: const EdgeInsets.all(4),
      child: Text(
        formatted,
        style: Theme.of(context)
            .textTheme
            .labelMedium!
            .copyWith(color: Colors.white),
      ),
    );
  }
}

initServices() {
  GetIt.I.registerSingleton(LogVault());
  GetIt.I.registerSingleton(SessionVault());
  GetIt.I.get<SessionVault>().init();
}

QAToolsGuardMain<TEnv extends IApplicationEnvironment>(
    List<TEnv> envs, Launcher<TEnv> launcher,
    [OnReloadCallback? onRestart]) {
  initServices();
  final options = ApplicationOptions<TEnv>(envs);
  WidgetsFlutterBinding.ensureInitialized();

  openDevtool<TEnv>(options, launcher, onRestart);
}
