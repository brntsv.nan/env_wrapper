import 'package:env_wrapper/data/monitoring_entries.dart';
import 'package:env_wrapper/data/session.dart';
import 'package:env_wrapper/services/log_vault.dart';
import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';

class SessionVault extends ChangeNotifier {
  final List<MonitoringSession> sessions = [];
  MonitoringSession? currentSession;

  late LogVault vault;

  void init() {
    vault = GetIt.I.get<LogVault>();
    vault.logs.listen(onNewLogEntry);
    startRecording();
  }

  void appRestarted() {
    startRecording();
  }

  void onNewLogEntry(MonitoringEntry entry) {
    currentSession!.entries.add(entry);
    notifyListeners();
  }

  void startRecording() {
    if (currentSession != null) {
      captureRecording();
    }
    currentSession = MonitoringSession();
    notifyListeners();
  }

  void captureRecording() {
    currentSession!.end = DateTime.now();
    sessions.add(currentSession!);
  }
}
