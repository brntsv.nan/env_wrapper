import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:stack_trace/stack_trace.dart';

import '../data/monitoring_entries.dart';

class LogVault extends ChangeNotifier {
  final logsStreamController = StreamController<MonitoringEntry>.broadcast();
  late Stream<MonitoringEntry> logs = logsStreamController.stream;

  void addEntry(MonitoringEntry entry) {
    entries.add(entry);
    logsStreamController.sink.add(entry);
    notifyListeners();
  }

  void addBlocCreateEntry(Object bloc) {
    addEntry(MonitoringEntry.blocEvent(
      sourceBlocType: bloc.runtimeType.toString(),
      type: BlocEventType.create,
      logTimestamp: DateTime.now(),
    ));
  }

  void addException(Object exception, StackTrace trace) {
    final t = Trace.from(trace);
    addEntry(MonitoringEntry.exception(
      object: exception,
      frames: [
        for (final trace in t.frames)
          StackFrame(
            funcName: trace.member??"",
            column: trace.column,
            line: trace.line,
            path: trace.uri.toString()
          ),
      ],
    ));
  }

  void addBlocEventEntry(Object bloc, Object? event) {
    addEntry(
      MonitoringEntry.blocEvent(
        sourceBlocType: bloc.runtimeType.toString(),
        type: BlocEventType.event,
        eventType: event.runtimeType.toString(),
        logTimestamp: DateTime.now(),
      ),
    );
  }

  void addBlocStateEntry(Object bloc, Object nextState) {
    addEntry(
      MonitoringEntry.blocEvent(
        sourceBlocType: bloc.runtimeType.toString(),
        type: BlocEventType.stateChange,
        eventType: nextState.runtimeType.toString(),
        logTimestamp: DateTime.now(),
      ),
    );
  }

  static List<MonitoringEntry> entries = [];
}
